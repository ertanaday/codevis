<?xml version="1.0" ?>
<!DOCTYPE refentry PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN" "dtd/kdedbx45.dtd" [
  <!ENTITY codevis "<acronym>codevis</acronym>">
  <!ENTITY Tomaz.Canabrava '<personname><firstname>Tomaz</firstname><surname>Canabrava</surname></personname>'>
  <!ENTITY Tomaz.Canabrava.mail '<email>tcanabrava@kde.org</email>'>
  <!ENTITY Tarcisio.Fischer '<personname><firstname>Tarcisio</firstname><surname>Fischer</surname></personname>'>
  <!ENTITY Tarcisio.Fischer.mail '<email>tarcisio.fischer@codethink.co.uk</email>'>
  <!ENTITY underApache "Apache 2.0 License">
  <!ENTITY % English "INCLUDE">
]>

<refentry lang="&language;">
<refentryinfo>
    <title>&codevis; User's Manual</title>
    <author>&Tomaz.Canabrava; &Tomaz.Canabrava.mail;</author>
    <date>2024-00-00</date>
    <author>&Tarcisio.Fischer; &Tarcisio.Fischer.mail;</author>
    <date>2023-10-19</date>
    <releaseinfo>KDE Gear 24.00</releaseinfo>
    <productname>KDE Gear</productname>
</refentryinfo>

<refmeta>
<refentrytitle><command>codevis_desktop</command></refentrytitle>
<manvolnum>1</manvolnum>
</refmeta>

<refnamediv>
<refname><command>codevis_desktop</command></refname>
<refpurpose>Large Scale Software Architectural Visualization by &kde;</refpurpose>
</refnamediv>

<refsynopsisdiv>
    <cmdsynopsis>
        <!-- add a list of command line arguments here. -->
        <command>codevis_desktop</command>
        <group choice="opt"><option>--help</option></group>
    </cmdsynopsis>
</refsynopsisdiv>

<refsect1>
<title>Description</title>
<para>&codevis; is the &kde; Large Scale Software Architectural analysis tool. </para>
<para>
  Some of &codevis;'s many features includes software architecture visualization,
  manipulation, code generation from diagrams, diagrams generation from source code,
  plugins, static analysis, visual static analysis and much more.
</para>

<para>
  But &codevis; is more than an architecture helper. Its ability to open
  several files at once makes it ideal for visualizing different architectures at once,
  and experimenting. Many architectural issues were found and fixed on &codevis; itself by
  analyzing it's source code.
</para>
</refsect1>

<refsect1>
    <title>Options</title>
    <variablelist>
        <!-- Add more entities here with longer explanations -->
        <varlistentry>
            <term><option>--help</option></term>
            <listitem><para>Shows the help and exits</para></listitem>
        </varlistentry>
    </variablelist>
</refsect1>

<refsect1>
<title>Plugins</title>
<para>
Codevis application supports plugins. It is possible to create plugins in C++, Python or mix the two languages.
</para>
<para>
The plugin system works as follows: There's a set of places in the application code that call user-definable functions.
 Those functions are called "Hooks". Each hook receives a pointer to a "handler" that'll be provided by the application.
 So a "hook" is a "place" _where_ the function will be called and the handler is _what_ can be called back to change
 application behavior.
</para>
<para>
All hooks are defined in the [hooks.py](../lvtplg/hooks.py) file. All handlers are defined in one of the handler files
in the [handlers.py](../lvtplg/handlers.py) file. Handlers may return application data structures, such as an "Entity".
Those are available in the [plugin data types file](../lvtplg/ct_lvtplg_plugindatatypes.h).
</para>
<para>
In order for the application to recognize the plugins, they need to be in a specific place and they need to have a
specific set of files. This is the file structure each plugin must have:
</para>
<para>
$plugin_name/
+ $plugin_name.[py|so]
+ metadata.json
+ README.md
</para>
<para>
Where `$plugin_name` is the plugin's name. `metadata.json` file follows the [kcoreaddons](https://api.kde.org/frameworks/kcoreaddons/html/)
specification (There's an example below). And the `README.md` file should contain a brief description, and
possibly examples on how to use the plugin.
</para>
<para>
The `$plugin_name` folder must be copied to one of those paths, so that the application can find the plugin:
</para>
<para>
- `$user_home/lks-plugins/` (Preferred for local development)
- `$app_installation_path/lks-plugins/` (Used for plugins that are bundled with the app)
- `$app_local_data/plugins/` (Used for downloaded plugins)
</para>
<para>
There are a few working examples in the [plugins folder](../plugins/). There's also some [plugins for integration test](../lvtplg/testplugins/)
that may be useful as simple examples. But just to give the
reader a more step-by-step explanation, this section will show a working plugin written in Python.
There's also a [python template plugin](../plugins/python_template_plugin/) that may be used as starting point.
</para>
<para>
**Step 1**: Create the following folder structure inside `$user_home/lks-plugins/` (example: `/home/tarcisio/lks-plugins/`):
</para>
<para>
```
myfirstplugin/
+ myfirstplugin.py
+ metadata.json
+ README.md
```
</para>
<para>
**Step 2**: Populate the `metadata.json` file with the following contents:
</para>
<para>
```
 {
  "KPlugin": {
     "Name": "My first plugin",
     "Description": "Example plugin",
     "Icon": "none",
     "Authors": [ { "Name": "YourName", "Email": "YourEmail" } ],
     "Category": "Codevis Plugins",
     "EnabledByDefault": true,
     "License": "YourLicense",
     "Id": "myfirstplugin",
     "Version": "0.1",
     "Website": "YourPluginWebsite"
  }
}
```
</para>
<para>
**Step 3**: Populate the `myfirstplugin.py` file with the actual plugin code:
</para>
<para>
```python
import pyLksPlugin as plg # Contains Codevis data structures

def hookSetupPlugin(handler):
    # This will be executed once when the application starts
    print("Hello world from Python plugin!")


def hookTeardownPlugin(handler):
    # This will be executed once when the application closes
    print("Okay then. Bye!")
```
</para>
<para>
**Step 4**: Save all files and start the Codevis application using your terminal. You should see the messages in the
console.
</para>
<para>
Codevis comes with a builtin plugin editor that can be used for editing the plugins. To access it just click on
`View > Plugin Editor`.
</para>
</refsect1>

<refsect1>
<title>See Also</title>

<simplelist><member>More detailed user documentation is available from <ulink
url="help:/codevis">help:/codevis</ulink>
(either enter this &URL; into a web-browser;, or run
<userinput><command>khelpcenter</command>
<parameter>help:/codevis</parameter></userinput>).</member>
<member>kf5options(7)</member>
<member>qt5options(7)</member>
<member>There is also further information available at the <ulink
url="https://invent.kde.org/sdk/codevis/">&codevis; website</ulink>.
</member>
</simplelist>
</refsect1>

</refentry>
